import { SET_TOKEN } from "../actions/actionTypes";
import isValid from "../utils/validation/is-valid";

const initialState = {
  isAuthenticated: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_TOKEN:
      return {
        ...state,
        isAuthenticated: isValid(action.payload)
      };
    default:
      return state;
  }
}
