import reducer from "./authReducers";
import * as types from "./../actions/actionTypes";

import { TOKEN } from "../utils/constants";

describe("Reducers", () => {
  it("should return the initial state", () => {
    expect(reducer(undefined, {})).toEqual({
      isAuthenticated: false
    });
  });

  it("should handle SET_TOKEN on login", () => {
    expect(
      reducer(
        {
          isAuthenticated: false
        },
        {
          type: types.SET_TOKEN,
          payload: TOKEN
        }
      )
    ).toEqual({
      isAuthenticated: true
    });
  });
});
