import React, { Component } from "react";
import { Helmet } from "react-helmet";

import { Provider } from "react-redux";
import store, { history } from "./store";

import { setToken, logoutUser } from "./actions/authActions";

import { ConnectedRouter } from "connected-react-router";
import routes from "./routes";

import * as moment from "moment";

import { library } from "@fortawesome/fontawesome-svg-core";
import { faEnvelope, faKey } from "@fortawesome/free-solid-svg-icons";

import "./App.scss";

library.add(faEnvelope, faKey);

if (localStorage.token) {
  const token = localStorage.token;
  store.dispatch(setToken(token));
  const currentTime = moment();

  // Logs out user after 5 mins
  if (localStorage.loginTime + moment().add(5, "m") < currentTime) {
    store.dispatch(logoutUser());
    window.location.href = "/login";
  }
}

class App extends Component {
  render() {
    return (
      <div>
        <Helmet>
          <meta charset="utf-8" />
          <title>Front-End Technical Test Application</title>
        </Helmet>
        <Provider store={store}>
          <ConnectedRouter history={history}>{routes}</ConnectedRouter>
        </Provider>
      </div>
    );
  }
}

export default App;
