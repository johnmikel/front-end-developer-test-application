import React from "react";
import { shallow } from "enzyme";
import PrivateRoute from "./PrivateRoute";

it("PrivateRoute component renders without crashing", () => {
  shallow(<PrivateRoute />);
});
