import React from "react";

export default props => {
  return (
    <div className="card">
      <div className="card-content">
        <div className="media">
          <div className="media-left">
            <figure className="image is-48x48">
              <img src={props.avatar} alt="Avatar" />
            </figure>
          </div>
          <div className="media-content">
            <p className="title is-4">{props.name}</p>
            <p className="subtitle is-6">@{props.id}</p>
          </div>
        </div>
      </div>
    </div>
  );
};
