import React, { Component } from "react";
import { connect } from "react-redux";

import axios from "axios";

import { logoutUser } from "../actions/authActions";

import Card from "./common/Card";

import { Logo } from "./../utils/styled components";
import logo from "./../assets/img/logo.svg";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: []
    };
  }

  componentDidMount() {
    this._loadUsers = axios
      .get("https://reqres.in/api/users?page=2")
      .then(res => {
        this._loadUsers = null;
        this.setState({ users: res.data.data });
      });
  }

  componentWillUnmount() {
    if (this._loadUsers) {
      this._loadUsers.cancel();
    }
  }

  onLogOut = () => {
    console.log("Logging out.");
    this.props.logoutUser();
  };

  render() {
    if (this.state.users === null) {
      return <h1>Loading...</h1>;
    } else {
      return (
        <div className="container">
          <nav
            className="navbar"
            role="navigation"
            aria-label="main navigation"
          >
            <div className="navbar-brand">
              <Logo src={logo} />
            </div>

            <div className="navbar-menu">
              <div className="navbar-end">
                <div className="navbar-item">
                  <div className="buttons">
                    <button onClick={this.onLogOut} className="button is-light">
                      Log Out
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </nav>

          <div className="columns">
            {this.state.users.map(user => (
              <div className="column" key={user.id}>
                <Card
                  id={user.id}
                  name={user.first_name + " " + user.last_name}
                  avatar={user.avatar}
                />
              </div>
            ))}
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(Dashboard);
