import React, { Component } from "react";
// import { reduxForm, Field } from "redux-form";
import { connect } from "react-redux";
import { loginUser } from "../actions/authActions";

import { Logo, LoginImageColumn, LoginFormColumn } from "../utils/styled components";

import logo from "./../assets/img/logo.svg";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: ""
    };
  }

  onSubmit = e => {
    e.preventDefault();

    const userData = {
      email: this.state.email,
      password: this.state.password
    };

    this.props.loginUser(userData);
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    return (
      <div className="columns">
        <LoginFormColumn className="column">
          <Logo src={logo} />
          <form onSubmit={this.onSubmit}>
            <div className="field">
              <div className="control has-icons-left has-icons-right">
                <input
                  className="input is-medium"
                  name="email"
                  type="email"
                  placeholder="Email"
                  value={this.state.email}
                  onChange={this.onChange}
                />
                <span className="icon is-medium is-left">
                  <FontAwesomeIcon icon="envelope" />
                </span>
              </div>
            </div>
            <div className="field">
              <div className="control has-icons-left has-icons-right">
                <input
                  className="input is-medium"
                  name="password"
                  type="password"
                  placeholder="Password"
                  value={this.state.password}
                  onChange={this.onChange}
                />
                <span className="icon is-medium is-left">
                  <FontAwesomeIcon icon="key" />
                </span>
              </div>
            </div>
            <div className="control">
              <input
                type="submit"
                value="Login"
                className="button is-medium is-pulled-right"
              />
            </div>
          </form>
        </LoginFormColumn>
        <LoginImageColumn className="column is-two-thirds" />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { loginUser }
)(Login);
