import { push } from "connected-react-router";

import axios from "axios";

import { SET_TOKEN, GET_ERRORS } from "./actionTypes";

import * as moment from "moment";

import { API_LOGIN } from "./../utils/constants";

export const loginUser = userData => dispatch => {
  axios
    .post(API_LOGIN, userData)
    .then(res => {
      const { token } = res.data;
      const loginTime = moment();

      // Save token in local storage

      localStorage.setItem("token", token);

      // the API returns only a token as a response after POST, so I am doing the session expiry
      // via localStorage (it's not a solution but it'll work for now)

      localStorage.setItem("loginTime", loginTime);

      // since the API only returns token upon successful login,
      // it is a little weird because all the tokens are the same
      dispatch(setToken(token));
      dispatch(push("/dashboard"));
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: "There was an error." //stub
      })
    );
};

export const setToken = token => {
  return {
    type: SET_TOKEN,
    payload: token
  };
};

export const logoutUser = () => dispatch => {
  localStorage.removeItem("token");
  dispatch(setToken({}));
  dispatch(push("/"));
};
