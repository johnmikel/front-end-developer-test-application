import * as actions from "./authActions";
import * as types from "./actionTypes";

describe("Actions", () => {
  it("should create an action to set the API's token on login", () => {
    const token = "QpwL5tke4Pnpja7X";
    const expectedAction = {
      type: types.SET_TOKEN,
      payload: token
    };
    expect(actions.setToken(token)).toEqual(expectedAction);
  });

  it("should create an action to destroy the API's token on logout", () => {
    const expectedAction = {
      type: types.SET_TOKEN,
      payload: undefined
    };
    expect(actions.setToken()).toEqual(expectedAction);
  });
});
