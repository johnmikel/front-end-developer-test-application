import styled from "styled-components";

import loginSplashImage from "./../../assets/img/loginSplashScreen.jpg";

const maxWindowHeight = "100vh";
const logoWidth = "400px";

export const Logo = styled.img`
  width: ${logoWidth};
`;

export const LoginFormColumn = styled.div`
  height: ${maxWindowHeight};
  margin: auto;
  padding: 3rem !important;
`;

export const LoginImageColumn = styled.div`
  background-image: url(${loginSplashImage});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  height: ${maxWindowHeight};
  margin: 0;
  padding: 0;
`;
