import React from "react";

import { Route, Switch } from "react-router-dom";

import PrivateRoute from "../components/common/PrivateRoute";

import Login from "./../components/Login";
import Dashboard from "../components/Dashboard";

const routes = (
  <div>
    <Route exact path="/" component={Login} />
    <Switch>
      <PrivateRoute exact path="/dashboard" component={Dashboard} />
    </Switch>
  </div>
);

export default routes;
