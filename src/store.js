import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension/logOnlyInProduction";

import { createBrowserHistory } from "history";
import { routerMiddleware } from "connected-react-router";

import thunk from "redux-thunk";

import rootReducer from "./reducers";

// import createSagaMiddleware from "redux-saga";
// import RootSaga from "./sagas";

// const sagaMiddleware = createSagaMiddleware();

const initialState = {};

export const history = createBrowserHistory();
const middleware = [thunk, routerMiddleware(history)];

const store = createStore(
  rootReducer(history),
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);

// sagaMiddleware.run(RootSaga);

export default store;
