# Front End Developer Technical Test Application

This project was created with Create React App.
Clone the project first then and do

### `npm install` / `yarn install`

This will install all dependencies.

After that, do:

### `npm start` / `yarn start`

This runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

###Time Spent

I've spent about 4 hours developing this fedta (cool acronym by the way) application. It was done in intervals for I had to do errands (e.g. Attending to the variety shop of my mother-in-law located at CR4 2JB, putting baby to sleep) in-between.

###Testing

Simply run

### `npm test` / `yarn test`

I did not implement advance testing concepts like async / await, only the basic ones. However, I am eager, interested and keen to learn more advance testing techniques should I be a part of the team as it helped me see a bug in the reducers.js and realize how important testing is (I'm more confident with my code this time). I had an initial state of key 'token' that I am not using at all - which I found through testing the reducers via Jest / Enzyme.

###Technical requirements and some comments

I've used Bulma for styling and incorporated node-sass to customize it with the help of Bulma's documented variables.

The API was a bit funny as it only returned a token upon successful login / registration of new user. It had no time stamp so I had to create a bogus time stamp (using localStorage) to make the expiration session 5 minutes.

The validation was also ridiculous, it's just an exported function made to compare the token with the API's token.

Summarizng, I've made some funny adjustments to the app so as I can conform with the API. I think, as a front end developer, it's a must to be flexible in such scenarios.

Thank you and I hope you enjoy.
